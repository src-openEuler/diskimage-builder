%global _empty_manifest_terminate_build 0
Name:       diskimage-builder
Version:    3.33.0
Release:    2
Summary:    olden Disk Image builder.
License:    Apache-2.0
URL:        https://docs.openstack.org/diskimage-builder
Source0:    https://files.pythonhosted.org/packages/source/d/diskimage-builder/diskimage-builder-%{version}.tar.gz

# Increase upper version to fix install because python-flake8 has been
# upgraded to 6.1.0
Patch0:     0001-Increase-the-upper-limit-version-of-flake8.patch

BuildArch:  noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  git
BuildRequires:  python3-pbr

Requires:       kpartx
Requires:       qemu-img
Requires:       curl
Requires:       tar
Requires:       gdisk
Requires:       git
Requires:       xfsprogs
Requires:	python3-PyYAML
Requires:	python3-flake8
Requires:	python3-networkx
Requires:	python3-pbr
Requires:	python3-stevedore

%description
``diskimage-builder`` is a flexible suite of components for building a
wide-range of disk images, filesystem images and ramdisk images for
use with OpenStack.

%package help
Summary:	Development documents and examples for diskimage-builder
Provides:	python3-diskimage-builder-doc
%description help
``diskimage-builder`` is a flexible suite of components for building a
wide-range of disk images, filesystem images and ramdisk images for
use with OpenStack.

%define __requires_exclude /sbin/runscript
%prep
%autosetup -n diskimage-builder-%{version} -p1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n diskimage-builder -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Sep 18 2024 zhangxianting <zhangxianting@uniontech.com> - 3.33.0-2
- Increase the upper limit version of flake8 to fix install, flake8<7.0.0 to flake8<=7.1.1

* Fri Jul 26 2024 warlcok <hunan@kylinos.cn> - 3.33.0-1
- Upgrade package python3-diskimage-builder to version 3.33.0
  A new fail2ban element.
  The bootloader element will explicitly set the timeout style to hidden to hide the menu.
  Fixes an issue: disable that by default with ConfigDrive argument.

* Tue Apr 02 2024 zhangxianting <zhangxianting@uniontech.com> - 3.31.0-2
- Increase the upper limit version of flake8 to fix install, flake8<7.0.0 to flake8<=7.0.0

* Fri Sep 08 2023 Dongxing Wang <dxwangk@isoftstone.com> - 3.31.0-1
- Upgrade package python3-diskimage-builder to version 3.31.0

* Fri May 12 2023 Han Guangyu <hanguangyu@uniontech.com> - 3.20.3-2
- Increase the upper limit version of flake8 to fix install

* Mon Jul 04 2022 OpenStack_SIG <openstack@openeuler.org> - 3.20.3-1
- Upgrade package python3-diskimage-builder to version 3.20.3

* Wed Jul 14 2021 liksh <liks11@chinaunicom.cn> - 3.7.0-1
- update from 3.2.1 to 3.7.0

* Sun Feb 07 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated

